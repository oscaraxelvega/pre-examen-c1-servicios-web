const getUsers = (id) => {
    axios.get('https://jsonplaceholder.typicode.com/users/'+id)
    .then(response => {
    const user = response.data;
    document.getElementById('nombre').value = user.name;
    document.getElementById('usuario').value = user.username;
    document.getElementById('correo').value = user.email;
    document.getElementById('domicilio').value = user.address.street+" "+user.address.suite+", "+user.address.city;
    })
     .catch(error => {
         console.error(error);
         alert("No se encuentra este Vendedor");
         limpiar();
        });
};

function limpiar(){
    document.getElementById('nombre').value = "";
    document.getElementById('usuario').value = "";
    document.getElementById('correo').value = "";
    document.getElementById('domicilio').value = "";
    document.getElementById('idVendedor').value  = "";
};

document.getElementById('btnCargar').addEventListener("click", function(){
    id = parseInt(document.getElementById('idVendedor').value);
    if(!id){
        alert("Ingrese un ID");
        limpiar();
    } else{
        getUsers(id);
    }
});

