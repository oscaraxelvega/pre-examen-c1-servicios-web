const cargarLista = async() => {
    const url = 'https://dog.ceo/api/breeds/list';
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        const list = document.getElementById('lista');
        resultado.message.forEach(element => {
            //console.log(element);
            let optionHTML = document.createElement("option");
            optionHTML.value = element;
            optionHTML.text = element;
            list.appendChild(optionHTML);
        });
    } catch (error) {
        console.error(error);
        alert('No se pudo cargar la lista de razas');
    }
}

const cargarImg = async() =>{
    const list = document.getElementById('lista');
    const raza = list.options[list.selectedIndex];
    if (list.selectedIndex != -1) {
        const url = 'https://dog.ceo/api/breed/'+raza.value+'/images/random'
        try {
            const respuesta = await fetch(url);
            const resultado = await respuesta.json();
            document.getElementById('imagen').style.backgroundImage = "url('"+resultado.message+"')";
        } catch (error) {
            console.error(error);
        }
    } else {
        alert('No se ha seleccionado ninguna raza para mostrar');
    }
}